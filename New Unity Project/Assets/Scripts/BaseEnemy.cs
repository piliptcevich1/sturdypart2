﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemy : MonoBehaviour, IEnemy, IHitBox
{
    
    public void RegisterEnemy()
    {
        GameManager manager = FindObjectOfType<GameManager>();
        manager.Enemies.Add(this); //каждый враг добавляется в массив врагов
    }
    
    [SerializeField] private int health = 1;
    public int Health 
    {
        get => health;

        private set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
       
    }
    public void Hit(int damage)
    {
        Health -= damage;
        Health = health;
        //еще здесь строки
    }

    public void Die()
    {
        print("Enemy Die");
    }
    private void Awake()
    {
        RegisterEnemy();
    }
}
