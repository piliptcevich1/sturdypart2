﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterMovement : MonoBehaviour //абстракный класс не будет реализован, но он может управлять чем-то .. можно использовать в разных компонентах
{
    public bool IsFreezing;
    public abstract void Move(Vector2 direction);
    public abstract void Stop(float timer);
    public abstract void Jump(float force);
    
}
