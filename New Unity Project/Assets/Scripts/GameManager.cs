﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    MainMenu,
    Loading,
    Game,
    GamePause,
    //описываем состояния игры
}

public class GameManager : MonoBehaviour
{
    private static GameState currentGameState;

    public static Action<GameState> GameStateAction; //у всех подписчиков игры будет виден статус и доступны действия этого статуса

    public static GameState GetGameState => currentGameState; //возможность вызова текущего статуса игры в других классах

    public IPlayer Player;
    public List<IEnemy> Enemies = new List<IEnemy>();
    
    public static void SetGameState(GameState newGameState)
    {
        currentGameState = newGameState;
        GameStateAction?.Invoke(newGameState); //когда вызывается статус, то он становится текущим
        //коррутина - оток действий, который идет одновременно с основным потоком игры .. сохранение логики даже если игрок его пока не выбрал
        //мы просто вызываем это событие, если нужно изменить геймстейт
    }

   private void Start()
    {
        //Debug.Log("Start");
        StartCoroutine(Wait()); //вызов коррутины
        SetGameState(GameState.Game);
        print(Player);
        print($"Enemies count{Enemies.Count}"); //показываем кто плеер и сколько у нас врагов
    }

//так работает коррутина - закладываем например таймер, проверяем нужно ли показывать какой-то флаг .. это экономнее, чем через абтейт .. всегда начинается с "I" и завершается yield (вместо return)
    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(3f);
        //Debug.Log("Hello world");
        //каррутина всегда существует, пока активен монобихев .. как только он прячется или удаляется, каррутины удаляются вместе с ним
        yield return new WaitForSeconds(1f);
        //Debug.Log("Hello world2");
        //коррутин прерывает вычисление до выполнения следующего действия .. посмотреть готовые предустановки коррутины помимо WaitForSeconds .. много заготовок для подобных обработчиков
    }
}
  
