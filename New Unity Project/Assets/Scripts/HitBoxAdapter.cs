﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitBoxAdapter : MonoBehaviour, IHitBox
{
    [SerializeField] private GameObject hitTarget;
    private IHitBox hitBox;
    private void Reset()
    {
        //когда нужно добавить компонент

        var hit = GetComponentInParent<IHitBox>();
        if (hit != null && hit != this)
        {
            hitTarget = (hit as MonoBehaviour)?.gameObject;
        }
    
    
    // Start is called before the first frame update
    void Start()
    {
        hitBox = hitTarget.GetComponent<IHitBox>();
    }


    public int Health => hitBox.Health;
    public void Hit(int damage)
    {
        hitBox.Hit(damage);
    }

    public void Die()
    {
        //пустые, т.к сам объект умереть не может, он передает свой хит другому
        hitBox.Die();
    }
}
