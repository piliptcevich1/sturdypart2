﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    public static float HorizontalAxis;

    private float jumpTime;
    private Coroutine waitForJumpCoroutine;
    public static event Action<float> JumpAction;
    public static Action<string> FireAction;
    
        
    private void Start()
    {
        HorizontalAxis = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        HorizontalAxis = Input.GetAxis("Horizontal"); // типовой инпут .. вызов горизонтальной оси движения
        if (Input.GetButton("Jump"))
        {
            if (waitForJumpCoroutine == null)
            {
                waitForJumpCoroutine = StartCoroutine(WaitJump());
                return;
            }
        }

        if (Input.GetButton("Fire1"))
        {
            FireAction?.Invoke("Fire1");
        }
        
        if (Input.GetButton("Fire2"))
        {
            FireAction?.Invoke("Fire2");
        }
    }

    private void OnDestroy()
    {
        HorizontalAxis = 0f;
    }

    private IEnumerator WaitJump()
    {
        yield return new WaitForSeconds(0.2f);
        if (JumpAction !=null)

        {
            float force = Time.time - jumpTime < 0.2f ? 1.25f: 1f; //считаем сколько нажатие .. енажали дважды за 0.2 секунды, тогда дабл джамп .. и прыжок будет 1,25 .. иначе простой прыжок 1
            JumpAction.Invoke(force); //прыгает любой, кто подписан на инпут, не важно .. в т.ч. игрок
        }

        waitForJumpCoroutine = null; //именно эту карутину нуно обнулить, иначе будет виссеть в памяти активной
    }
}
