﻿public interface IPlayer
{
    void RegisterPlayer();
}

public interface IEnemy
{
    void RegisterEnemy();
}

public interface IDamager
{
    int Damage { get; } //поля нельзя в интерфейса, но можно вписывать свойства get - Доступно только для чтения
    void SetDamage();
}

public interface IHitBox
{
    int Health { get; }
    void Hit(int damage);
    void Die();
}