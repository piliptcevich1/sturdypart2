﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenu : MonoBehaviour
{
    
   
    void Start()
    {
        GameManager.SetGameState(GameState.MainMenu); //заменили статус игры на мейнменю
    }

    public void LoadLevel(string level)
    {
        SceneLoader.LoadLevel(level);
    }
}
