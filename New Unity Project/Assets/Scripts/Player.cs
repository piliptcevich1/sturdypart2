﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Player : MonoBehaviour, IPlayer, IHitBox  //сразу выдаст ошибку - нажать на лампочку и выбрать первую строчку .. тогда свяжутся плеер и монобихев
{
    private PlayerWeapon[] weapons;
   public void RegisterPlayer()
    {
        //throw new System.NotImplementedException();
        GameManager manager = FindObjectOfType<GameManager>();
        
        if (manager.Player == null)
        {
            manager.Player = this; //как только нашли, его назначаем классу
        }
        else
        {
            Destroy(gameObject); //если продублировался плеер, то нужно его уничтожить
        }
    }

   [SerializeField] private int health = 1;
   public int Health 
   {
       get => health;

        private set
        {
            health = value;
            if (health <= 0)
            {
                 Die();
            }
        }
       
   }
   public void Hit(int damage)
   {
       Health -= damage;
   }

   public void Die()
   {
       print("Player Die");
   }

   private void OnDestroy()
   {
       InputController.FireAction -= Attack;
   }

   private void Attack(string button)
   {
       foreach(var weapon in weapons)
       {
           if (weapon.ButtonName == button)
           {
               weapon.SetDamage();
           }
       }
   }

   private void Awake()
    {
        RegisterPlayer();
        weapons = GetComponents<PlayerWeapon>();
        InputController.FireAction += Attack;
    }
}

