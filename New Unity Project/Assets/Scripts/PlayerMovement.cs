﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))] //класс запрашивает компоненты у других класссов, находит его и использует
public class PlayerMovement : CharacterMovement

//посмотреть чем отличается абстрактный клас от интерфейса
{
    [SerializeField] private float maxSpeed = 5f;
    [SerializeField] private Transform grahics;
    [SerializeField] private float jumpForce = 5f;
    [SerializeField] private Animator animator;
    private Rigidbody2D rig;
    private static readonly int Speed = Animator.StringToHash("Speed");


    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();  //мы знаем в старте что он будет, но его нужно вызвать .. здесь вызываем
        InputController.JumpAction += OnJumpAction;
    }

    private void OnDestroy()
    {
        //отписка когда объект отключился
        InputController.JumpAction -= OnJumpAction;
    }

    private void FixedUpdate()
    {
        if (IsFreezing)
        {
            Vector2 velocity = rig.velocity;
            velocity.x = 0f;
            rig.velocity = velocity;
            return;
        }

        
        //апдейт вызывается каждый кадр, а фиксиапдейт только нужное количество кадров .. его используют для работы с физикой .. по умолчанию 0.02 ( т.е. 50 секунд)
        //настраивается в Project Settings .. Time .. в параметры физики .. 0.01 будет круче
        Vector2 direction = new Vector2(InputController.HorizontalAxis, 0f);
        
        if (!IsGrounded())
        {
            direction *= 0.5f;
        }
        
        Move(direction);
    }

    private void Update()
    {
        if (IsGrounded())
        {
            animator.SetFloat(Speed, Mathf.Abs(rig.velocity.x)); //Speed создается в юнити в параметрах аниматора .. добавляется и называется
        }
        else
        {
            animator.SetFloat(Speed, 0);
        }
        
        //здесь идет отрисовка обычных объектов, в частности будет показана физика, которая рассчитана с фикстапдейтом
        if (Mathf.Abs(rig.velocity.x) < 0.01f)  //velocity - вектор скорости (длина вектора, направление и время)  ... speed - линейная скорость (только время)
        {
            return; //если объект не двигается, то ничего не делаем
        }

        var angle = rig.velocity.x > 0f ? 0f : 180f;  //посмотреть конструкцию
       // var text = (float) xAngle;  запись приводящая один тип переменной в другой
       grahics.localEulerAngles = new Vector3(0f, angle, 0f);
    }

    public override void Move(Vector2 direction)
    {
        Vector2 velocity = rig.velocity;
        velocity.x = direction.x * maxSpeed;
        rig.velocity = velocity;
    }

    public override void Stop(float timer)
    {
        throw new System.NotImplementedException();
    }

    public override void Jump(float force)
    {
        rig.AddForce(new Vector2(0, force), ForceMode2D.Impulse); //импульс для дополнительной силы в моменты прыжка
    }

    private void OnJumpAction(float force)
    {
        //реагирует на нажатие прыжка
        if (IsGrounded() && !IsFreezing)
        {
            Jump(jumpForce * force);
        }
    }

    private bool IsGrounded()
    {
        //прыгнуть можно только стоя на земле . .проверяем есть ли снизу коллаидр
        Vector2 point = transform.position;
        point.y -= 0.1f; //чтобы избежать пересечение с самим собой
        RaycastHit2D
            hit = Physics2D.Raycast(point, Vector2.down,
                0.2f); //если он куда-то попал (первое пересечение), тогда переменная сохранит параметры рейкаста, иначе ноль
        return hit.collider != null;
    }
}
