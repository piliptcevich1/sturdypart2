﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour, IDamager
{

    [SerializeField] private WeaponData weaponData;
    [SerializeField] private Transform attackPoint; //точка из которой будет начинаться выстрел
    [SerializeField] private string buttonName = "Fire1";
    [SerializeField] private Animator animator;

    private float lastAttackTime; //время для контроля времени перезарядки
    public int Damage => weaponData.WeaponDamage;
    
    
    public void SetDamage()
    {

        if (Time.time - lastAttackTime < weaponData.FireRate)
        {
            return;
        }

        lastAttackTime = Time.time;
        
        animator.SetTrigger(Attack);
        
            
        var target = GetTarget();
        target?.Hit(Damage);
    }

    public string ButtonName => buttonName;

    private IHitBox GetTarget()
    {
        //если нажали на кнопку атаки, пытаемся определить, есть ли цель для атаки
        IHitBox target = null;
        RaycastHit2D hit = Physics2D.Raycast(attackPoint.position, attackPoint.right, weaponData.WeaponRange);  //право это вперед по ходу игрока, т.к. мы его разворачиваем вокруг своей оси, когда он поворачивает назад

        if (hit.collider !=null)
        {
            target = hit.transform.gameObject.GetComponent<IHitBox>(); 
            //отправляем в какой-то компонент, в котором есть интерфейс IHitBox  в рамках заданной длины выстрела .. цель первый попавший
        }
        
    }
    
}
