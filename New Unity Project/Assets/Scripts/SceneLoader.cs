﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneLoader : MonoBehaviour
{
    private static string nextLevel;

    public static void LoadLevel(string level)
    {
        nextLevel = level;
        SceneManager.LoadScene("LoadingScene"); //грузим сцену с именем LoadingScene
    }

    //пропадание через фейд аут .. ну, почти получилось ..
    private float Duration = 0.4f;
    
    private IEnumerator DoFade(CanvasGroup canvasGroup, float start, float end)
    {
        float counter = 0.2f;
        
        while (counter < Duration)
        {
            counter += Time.deltaTime * 0.2f;
            canvasGroup.alpha = Mathf.Lerp(start, end, counter / Duration);
            yield return null;
        }
    }
    
    private bool mFaded = false;
    
    private void Faid()
    {
        var canvGroup = GetComponent<CanvasGroup>();
        StartCoroutine(DoFade(canvGroup, canvGroup.alpha, mFaded ? 1 : 0));
        //print(canvGroup.alpha);
        mFaded = !mFaded;
    }
    // завершение эксперимента с пропаданием 
    
    
    //эксперимент с загрузкой уровня
    public GameObject loadingScreen;
    public Slider slider;
    
    
    //конец эксперимента с загрузкой уровня
    
    
    
    //старт можно оформить как карутин

    private IEnumerator Start()
    {
        GameManager.SetGameState(GameState.Loading);
        yield return new WaitForSeconds(1f);
        //часто лодинги делаются для того, чтобы игрок мог отдохнуть, но при этом может быть игра уже загружена

        if (string.IsNullOrEmpty(nextLevel)) //проверяем готовой функцией пустая ли строка
        {
            SceneManager.LoadScene("MainMenu");
            yield break; //прерываем игру
        }
        //создаем ассинхронную загрузку сцены .. можно загружть сцену состоящую из нескольких сцен
        AsyncOperation loading = SceneManager.LoadSceneAsync(nextLevel,
            LoadSceneMode.Additive); //Additive - подгрузка новой сцены в дополнение к основной
        while (!loading.isDone) //пока не завершится действие loading
        {
            //можно даже показать экран загрузки .. и спрашивать у загрузки ее прогресс loading.progress  и показывать %%
            print("loading progress");
            //Debug.Log(loading.progress);
            loadingScreen.SetActive(true);
            //float lprogress = Mathf.Clamp01(loading.progress / .9f);
            slider.value = loading.progress;
            print(loading.progress);
            
            
            
            yield return null; // ничего не делаем, ждем
        }

        Faid();
        
        nextLevel = null;
        SceneManager.UnloadSceneAsync("LoadingScene"); //мы убираем сцену загрузки (с %%)

    }
}
