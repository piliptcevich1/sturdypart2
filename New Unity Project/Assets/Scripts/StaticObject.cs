﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticObject : MonoBehaviour, IHitBox
{
    [SerializeField] private LevelObjectData objectData;  // здоровье будем брать из поля даты
    private Rigidbody2D rig;
    public int Health 
    {
        get => Health;

        private set
        {
            Health = value;
            
            if (Health <= 0)
            {
                Die();
            }
        }
       
    }
    public void Hit(int damage)
    {
        Health -= damage;
    }

    public void Die()
    {
        print("Object Die");
    }

    private void Start()
    {
        rig = GetComponent<Rigidbody2D>();
        Health = objectData.Health;
        if (rig)
        {
            rig.bodyType = objectData.Static ? RigidbodyType2D.Dynamic : RigidbodyType2D.Static; //меняем настройку рида
        }
    }

    [ContextMenu("Rename")]  //нужно чтоы ренейм заработал
    private void Rename()  //нужен для обращения к полям даты
    {
        if (objectData != null)
        {
            gameObject.name = objectData.name;
        }
    }

    [ContextMenu("CopyUp")]
    private void CopyUp()
    {
        var copy = Instantiate(gameObject);
        var pos = copy.transform.position;
        pos.y += 4;
        copy.transform.position = pos;
        //код можно использовать даже в редакторе - позволяет расставить объекты на карте автоматом

    }
}
